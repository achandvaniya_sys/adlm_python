# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

ADLM is Auritas data lifecycle management application, which implements retention compliance and enforces legal holds on data stored on the Hadoop
platform. It helps organizations implement defensible disposition.

### How do I get set up? ###

1) Install all the dependencies in requirements.txt
	pip install -r requirements.txt
2) Migrate db models:
	python manage.py migrate
	

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact